import Vue from 'vue'
import Router from 'vue-router'
import CardPokemon from '@/components/CardPokemon'
import Home from '@/pages/Home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
  ]
})
